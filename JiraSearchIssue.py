from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re

class JiraSearchIssue(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.implicitly_wait(30)
        self.base_url = "https://jira.atlassian.com/"
        self.verificationErrors = []
        self.accept_next_alert = True

    def test_jira_search_issue(self):
        driver = self.driver
        driver.get(self.base_url + "/secure/Dashboard.jspa")
        driver.find_element_by_link_text("Log In").click()
        driver.find_element_by_id("username").clear()
        driver.find_element_by_id("username").send_keys("malcolm_carson@hotmail.com")
        driver.find_element_by_id("password").clear()
        driver.find_element_by_id("password").send_keys("malcster75")
        driver.find_element_by_id("rememberMe").click()
        driver.find_element_by_id("login-submit").click()
        driver.find_element_by_id("find_link").click()
        driver.find_element_by_id("issues_new_search_link_lnk").click()
        driver.find_element_by_link_text("Advanced").click()
        driver.find_element_by_id("advanced-search").clear()
        driver.find_element_by_id("advanced-search").send_keys("issuetype = bug and key = \"TST-57113\"")
        driver.find_element_by_link_text("Test to create an issue").click()
        driver.find_element_by_id("vote-data").click()
        driver.find_element_by_xpath("//div[@id='inline-dialog-voters']/div/div/form/a").click()

    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException, e: return False
        return True

    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException, e: return False
        return True

    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True

    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
