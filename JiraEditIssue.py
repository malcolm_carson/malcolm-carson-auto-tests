# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re

class JiraEditIssue1(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = "https://jira.atlassian.com/"
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_jira_edit_issue1(self):
        driver = self.driver
        driver.get(self.base_url + "/secure/Dashboard.jspa")
        driver.find_element_by_id("header-details-user-fullname").click()
        driver.find_element_by_id("find_link").click()
        driver.find_element_by_id("find_link").click()
        driver.find_element_by_id("issues_new_search_link_lnk").click()
        driver.find_element_by_link_text("Advanced").click()
        driver.find_element_by_id("advanced-search").clear()
        driver.find_element_by_id("advanced-search").send_keys("issuetype = bug and key = \"TST-57113\"")
        driver.find_element_by_id("layout-switcher-button").click()
        driver.find_element_by_link_text("List View").click()
        driver.find_element_by_link_text("Test to create an issue").click()
        driver.find_element_by_css_selector("span.trigger-label").click()
        driver.find_element_by_id("description").clear()
        driver.find_element_by_id("description").send_keys("This is a test to edit an issue.\n\nThe next test will be to search for an issue")
        driver.find_element_by_id("edit-issue-submit").click()
    
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException, e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException, e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
