from selenium import webdriver

from selenium.webdriver.support.ui import WebDriverWait

import unittest

class LoginTest(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.get("https://id.atlassian.com/login/login?application=jac&continue=https://jira.atlassian.com/secure/Dashboard.jspa")

    def test_Login(self):
        driver = self.driver
        jiraUsername        = "malcolm_carson@hotmail.com"
        jiraPassword        = "malcster75"
        emailFieldID        = "username"
        passFieldID         = "password"
        loginButtonXpath    = "//input[@value='Log in']"

        emailFieldElement   = WebDriverWait(driver, 10).until(lambda driver: driver.find_element_by_id(emailFieldID))
        passFieldElement    = WebDriverWait(driver, 10).until(lambda driver: driver.find_element_by_id(passFieldID))
        loginButtonElement  = WebDriverWait(driver, 10).until(lambda driver: driver.find_element_by_xpath(loginButtonXpath))


        emailFieldElement.clear()
        emailFieldElement.send_keys(jiraUsername)
        passFieldElement.clear ()
        passFieldElement.send_keys (jiraPassword)
        loginButtonElement.click()
        WebDriverWait(driver, 10).until(lambda driver: driver.find_element_by_xpath(loginButtonXpath))

    def tearDown(self):
        self.driver.quit()

if __name__ == '__main__':
    unittest.main()
