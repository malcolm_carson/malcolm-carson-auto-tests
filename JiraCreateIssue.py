from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re

class AtlassianCreate1(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.implicitly_wait(30)
        self.base_url = "https://jira.atlassian.com/"
        self.verificationErrors = []
        self.accept_next_alert = True

    def test_atlassian_create1(self):
        driver = self.driver
        driver.get(self.base_url + "/secure/Dashboard.jspa")
        driver.find_element_by_link_text("Log In").click()
        driver.find_element_by_id("username").clear()
        driver.find_element_by_id("username").send_keys("malcolm_carson@hotmail.com")
        driver.find_element_by_id("password").clear()
        driver.find_element_by_id("password").send_keys("malcster75")
        driver.find_element_by_id("rememberMe").click()
        driver.find_element_by_id("login-submit").click()
        driver.find_element_by_id("create_link").click()
        driver.find_element_by_id("summary").clear()
        driver.find_element_by_id("summary").send_keys("test to create an issue 1")
        driver.find_element_by_id("description").clear()
        driver.find_element_by_id("description").send_keys("test env test")
        driver.find_element_by_id("create-issue-submit").click()
        driver.find_element_by_css_selector("span.aui-icon.icon-close").click()

    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException, e: return False
        return True

    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException, e: return False
        return True

    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True

    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
